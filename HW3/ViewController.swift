//
//  ViewController.swift
//  HW3
//
//  Created by Nazar Starantsov on 11/10/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let greetingKey = "greeting"
        
        Storage.set(value: "Hello", forKey: greetingKey)
        let readResult = Storage.getValue(forKey: "2")
        
        switch readResult {
        case .success(let recievedValue):
            guard let castedValue = recievedValue as? String else { break }
            print(castedValue)
        case .failure(let error):
            print(error.localizedDescription)
        }
    }


}

