//
//  Storage.swift
//  HW3
//
//  Created by Nazar Starantsov on 11/10/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation

final class Storage {
    public static func set<T>(value: T, forKey key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    public static func getValue(forKey key: String) -> Result<Any, Error> {
        do {
            guard let recievedValue = UserDefaults.standard.object(forKey: key) else {
                throw StorageError.readError(key)
            }
            
            return .success(recievedValue)
        } catch {
            return .failure(error)
        }
        
    }
    
    enum StorageError: Error, LocalizedError {
        case readError(String)
        case castingWhileReadError(String)
        
        var errorDescription: String? {
            switch self {
            case .readError(let key):
                return "Ошибка чтения значения по ключу: '\(key)'"
            case .castingWhileReadError(let castingType):
                return "Ошибка преобразования прочитанного значения в тип: \(castingType)"
            }
        }
    }
    
}
